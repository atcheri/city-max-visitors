package main

import (
	"fmt"
	"sort"
	"strings"

	"gitlab.com/atcheri/coderbyte-cel/traffic"
)

func CityTraffic(strArr []string) string {
	area := traffic.NewArea(strArr)
	cityIDs := make([]int, 0)
	for id := range area.Cities {
		cityIDs = append(cityIDs, id)
	}
	sort.Ints(cityIDs)
	str := make([]string, len(cityIDs))
	for i, cityID := range cityIDs {
		str[i] = fmt.Sprintf("%d:%d", cityID, area.MaxCityVisitors(cityID))
	}
	return strings.Join(str, ",")
}

func main() {
	inputs1 := []string{"1:[5]", "2:[5]", "3:[5]", "4:[5]", "5:[1,2,3,4]"}
	inputs2 := []string{"1:[5]", "2:[5,18]", "3:[5,12]", "4:[5]", "5:[1,2,3,4]", "18:[2]", "12:[3]"}
	inputs3 := []string{"1:[5]", "2:[5]", "3:[5]", "4:[5]", "5:[1,2,3,4]"}
	fmt.Println(CityTraffic(inputs1))
	fmt.Println(CityTraffic(inputs2))
	fmt.Println(CityTraffic(inputs3))
}
