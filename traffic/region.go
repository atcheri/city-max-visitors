package traffic

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
	"sync"
)

type Area struct {
	Cities map[int][]int
}

func NewArea(inputs []string) Area {
	// cities := make(map[int][]int, len(inputs))
	area := Area{Cities: map[int][]int{}}
	for _, input := range inputs {
		pop, conns := area.addCityConns(input)
		area.Cities[pop] = conns
		for _, connPop := range conns {
			if _, ok := area.Cities[connPop]; !ok {
				area.Cities[connPop] = []int{}
			}
		}
	}
	return area
}

func (a *Area) addCityConns(input string) (int, []int) {
	splits := strings.Split(input, ":")
	pop, err := strconv.Atoi(splits[0])
	if err != nil {
		panic(fmt.Sprintf("Can't convert to id %v", err))
	}
	tmp := splits[1]
	strConns := strings.Split(tmp[1:len(tmp)-1], ",")
	conns := make([]int, len(strConns))
	for i, c := range strConns {
		connId, err := strconv.Atoi(c)
		if err != nil {
			panic(fmt.Sprintf("Can't convert to connId %v", err))
		}
		conns[i] = connId
	}
	return pop, conns
}

func (a *Area) CityVisitors(id, from int) int {
	visitors := 0
	visitedCities := make(map[int]struct{})
	visitedCities[id] = struct{}{}
	visitedCities[from] = struct{}{}
	citiesToTraverse := []int{id}

	for {
		if len(citiesToTraverse) == 0 {
			break
		}

		visiting := citiesToTraverse[0]
		citiesToTraverse = citiesToTraverse[1:]
		conns, ok := a.Cities[visiting]
		if !ok {
			panic(fmt.Sprintf("No city for this id %d", id))
		}
		for _, cId := range conns {
			if _, ok := visitedCities[cId]; !ok {
				citiesToTraverse = append(citiesToTraverse, cId)
				visitedCities[cId] = struct{}{}
				visitors += cId
			}

		}
	}

	return visitors
}

func (a *Area) MaxCityVisitorsWithGoRoutines(id int) int {
	conns := a.Cities[id]
	wg := sync.WaitGroup{}
	wg.Add(len(conns))
	visitisCh := make(chan int)
	for _, conn := range conns {
		go func(cId int, waitGroup *sync.WaitGroup) {
			defer wg.Done()
			visitisCh <- cId + a.CityVisitors(cId, id)
		}(conn, &wg)
	}
	go func() {
		wg.Wait()
		close(visitisCh)
	}()

	max := 0
	for count := range visitisCh {
		if count > max {
			max = count
		}
	}

	if id == 5 {
		fmt.Println("5")
	}

	return max
}

func (a *Area) MaxCityVisitors(id int) int {
	conns := a.Cities[id]
	res := make([]int, 0)
	for _, conn := range conns {
		visitors := conn + a.CityVisitors(conn, id)
		res = append(res, visitors)
	}
	sort.Ints(res)

	return res[len(res)-1]
}
