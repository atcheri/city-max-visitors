package traffic

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestArea_NewArea(t *testing.T) {
	testCases := []struct {
		inputs   []string
		expected Area
		desc     string
	}{
		{
			inputs:   []string{"1:[5]"},
			expected: Area{Cities: map[int][]int{1: {5}, 5: {}}},
			desc:     "With input = \"1:[5]\"",
		},
		{
			inputs:   []string{"1:[5]", "4:[5]"},
			expected: Area{Cities: map[int][]int{1: {5}, 4: {5}, 5: {}}},
			desc:     "With input = \"1:[5]\", \"4:[5]\"",
		},
		{
			inputs:   []string{"1:[5]", "4:[5]", "3:[5]"},
			expected: Area{Cities: map[int][]int{1: {5}, 4: {5}, 3: {5}, 5: {}}},
			desc:     "With input = \"1:[5]\", \"4:[5]\", \"3:[5]\"",
		},
		{
			inputs:   []string{"1:[5]", "4:[5]", "3:[5]", "5:[1,4,3,2]"},
			expected: Area{Cities: map[int][]int{1: {5}, 4: {5}, 3: {5}, 5: {1, 4, 3, 2}, 2: {}}},
			desc:     "With input = \"1:[5]\", \"4:[5]\", \"3:[5]\", \"5:[1,4,3,2]\"",
		},
		{
			inputs:   []string{"1:[5]", "4:[5]", "3:[5]", "5:[1,4,3,2]", "2:[5,15,7]"},
			expected: Area{Cities: map[int][]int{1: {5}, 4: {5}, 3: {5}, 5: {1, 4, 3, 2}, 2: {5, 15, 7}, 7: {}, 15: {}}},
			desc:     "With input = \"1:[5]\", \"4:[5]\", \"3:[5]\", \"5:[1,4,3,2]\", \"2:[5,15,7]\"",
		},
		{
			// full first sample example
			inputs:   []string{"1:[5]", "4:[5]", "3:[5]", "5:[1,4,3,2]", "2:[5,15,7]", "7:[2,8]", "8:[7,38]", "15:[2]", "38:[8]"},
			expected: Area{Cities: map[int][]int{1: {5}, 4: {5}, 3: {5}, 5: {1, 4, 3, 2}, 2: {5, 15, 7}, 7: {2, 8}, 8: {7, 38}, 15: {2}, 38: {8}}},
			desc:     "With input = \"1:[5]\", \"4:[5]\", \"3:[5]\", \"5:[1,4,3,2]\", \"2:[5,15,7]\", \"7:[2,8]\", \"8:[7,38]\", \"15:[2]\", \"38:[8]\"",
		},
	}

	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			area := NewArea(tC.inputs)
			assert.Equal(t, area, tC.expected)
		})
	}
}

func TestArea_CityVisitors(t *testing.T) {
	testCases := []struct {
		inputs   []string
		cityId   int
		expected int
		desc     string
	}{
		{
			inputs:   []string{"1:[5]"},
			cityId:   1,
			expected: 5,
			desc:     "Visitors to city 1 With input = \"1:[5]\"",
		},
		{
			inputs:   []string{"1:[5]", "4:[5]"},
			cityId:   4,
			expected: 5,
			desc:     "Visitors to city 4 With input = \"1:[5]\", \"4:[5]\"",
		},
		{
			inputs:   []string{"1:[5]", "4:[5]", "3:[5]"},
			cityId:   3,
			expected: 5,
			desc:     "Visitors to city 3 With input = \"1:[5]\", \"4:[5]\", \"3:[5]\"",
		},
		{
			inputs:   []string{"1:[5]", "4:[5]", "3:[5]", "5:[1,4,3,2]"},
			cityId:   5,
			expected: 10,
			desc:     "Visitors to city 5 With input = \"1:[5]\", \"4:[5]\", \"3:[5]\", \"5:[1,4,3,2]\"",
		},
		{
			inputs:   []string{"1:[5]", "4:[5]", "3:[5]", "5:[1,4,3,2]", "2:[5,15,7]"},
			cityId:   2,
			expected: 35,
			desc:     "Visitors to city 2 With input = \"1:[5]\", \"4:[5]\", \"3:[5]\", \"5:[1,4,3,2]\", \"2:[5,15,7]\"",
		},
		{
			inputs:   []string{"1:[5]", "4:[5]", "3:[5]", "5:[1,4,3,2]", "2:[5,15,7]", "7:[2,8]", "8:[7,38]", "15:[2]", "38:[8]"},
			cityId:   7,
			expected: 76,
			desc:     "Visitors to city 2 With input = \"1:[5]\", \"4:[5]\", \"3:[5]\", \"5:[1,4,3,2]\", \"2:[5,15,7]\", \"7:[2,8]\", \"8:[7,38]\", \"15:[2]\", \"38:[8]\"",
		},
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			area := NewArea(tC.inputs)
			assert.Equal(t, tC.expected, area.CityVisitors(tC.cityId, -1))
		})
	}
}

func TestArea_MaxCityVisitors(t *testing.T) {
	area := NewArea([]string{"1:[5]", "4:[5]", "3:[5]", "5:[1,4,3,2]", "2:[5,15,7]", "7:[2,8]", "8:[7,38]", "15:[2]", "38:[8]"})
	testCases := []struct {
		cityId   int
		expected int
		desc     string
	}{
		{
			cityId:   1,
			expected: 82,
			desc:     "Max visitors to city 1",
		},
		{
			cityId:   2,
			expected: 53,
			desc:     "Max visitors to city 2",
		},
		{
			cityId:   5,
			expected: 70,
			desc:     "Max visitors to city 2",
		},
		{
			cityId:   7,
			expected: 46,
			desc:     "Max visitors to city 2",
		},
		{
			cityId:   38,
			expected: 45,
			desc:     "Max visitors to city 2",
		},
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			assert.Equal(t, tC.expected, area.MaxCityVisitors(tC.cityId))
		})
	}
}
